import numpy as np
import random as rd
import matplotlib.pyplot as plt


def generate_hill_village(rows, cols, num_hills, max_height,house_prob):
    '''
    In this code, we create a 3D list to represent the village. The height of each cell is determined by its distance to the peak of a randomly placed hill. Finally, houses are added to the village by raising the height of the surrounding cells.
    arg rows : int - number of rows
    arg cols : int - number of cols
    arg num_hills : int - number of hills generated
    arg max_height : float- the max height of a hill
    arg house_prob : float or int - probability of a house being generated on top of a hill
    
    returns :
    - the randomly generated village with the different parameters entered
    '''
    # Create the hill terrain matrix with each element as a list [height, 0]
    hill_village = [[[0, 0] for _ in range(cols)] for _ in range(rows)]
    peaks_row= []
    peaks_col = []

    for number_of_hills in range(num_hills):
        # Randomly place hills
        hill_center_row = rd.randint(0, rows - 1)
        hill_center_col = rd.randint(0, cols - 1)
        hill_radius = rd.uniform(20,30)
        hill_height = rd.uniform(1, max_height)
        peaks_col.append(hill_center_col)
        peaks_row.append(hill_center_row)


        for i in range(rows):
            for j in range(cols):
                distance_to_center = np.sqrt((i - hill_center_row)**2 + (j - hill_center_col)**2)
                if distance_to_center <= hill_radius:
                    # Adjust the height based on the distance to the center
                    height_change = hill_height * (1 - distance_to_center / hill_radius)
                    hill_village[i][j][0] += height_change
        
        
    # Ensure valid heights

    hill_village = np.clip(hill_village, 0, 5)

    # Place houses around hills
    for k in range (num_hills):
        if rd.random() < house_prob:
            for i in range(max(0, peaks_row[k] - 2), min(rows, peaks_row[k] + 3)):
                for j in range(max(0, peaks_col[k] - 2), min(cols, peaks_col[k] + 3)):
                    hill_village[i][j][0] += 5
        
    return hill_village

#function that writes the full matrix in a text file because the matrix is too large when trying to add a matrix to pre-made dictionary cities
def write_city(city):
    with open("hill2.txt","w") as f:    
        #the function np.clip changes the aspect of the matrix using ... instead of printing the whole matrix, so we have to use this code to get the proper aspect of a matrix
        f.write("[")
        for row in city:    
            line = "[{}]".format(", ".join([f"[{value[0]:.8f}, {value[1]}]" for value in row]))
            f.write(line + ",\n")
        f.write("]")
import copy
import numpy as np
import random as rd
from numpy import isclose
import matplotlib.pyplot as plt
import matplotlib.animation as animation

def generate_flat_city(length, width):
    '''
    function creates a completely empty city matrix
    
    Args :
    - length (int): length of the matrix (y axis - rows)
    - width (int): width of the matrix (x axis - columns)
    
    Returns:
    - city (matrix/numpy array) : empty (0 meter height) city matrix
    '''
    city=[]
    for _ in range(0,length):
        ligne = [0 for _ in range(0, width)]
        city.append(ligne)
    return city

def empty_city(city):
    '''
    function generate_flat_city creates a flat city of length and width with all values initially set to 0. It creates a list city to store the flat city and then iterates over length to create each row. Each row is represented as a list ligne with all values initially set to 0. Finally, the function returns the city list.
    
    Args:
    - city (matrix/numpy array) : current city matrix
    
    Returns:
    - city (matrix/numpy array) : City matrix with random heights all over
    '''
    for i in range(0, len(city)):
        for j in range(0, len(city[0])):
            city[i][j] = rd.randint(0,40)                          #40 meter max height
    return city
    
def vol_by_case(V, city):         
    '''
    Calculate volume per case.
    
    Args:
    - V (float) : volume of water that falls on the entire surface
    - city (matrix/numpy array) : a 2D list representing the city's layout
    
    Returns:
    V_by_case (float) : the amount of volume needed to put on each case
    '''
    V_by_case = V/(len(city)*len(city[0]))
    return V_by_case
    
def filled_city(city, V):              # City after flooding
    '''
    Filles city with the volume of water entered 
    
    Args:
    - city (matrix/numpy array) : Current city matrix
    - V (float)
    
    Returns:
    - city (matrix/numpy array) : current city matrix filled with the volume of water entered on top of it
    '''
    for i in range(0, len(city)):   # For each row in city
        for j in range(0, len(city[0])):    # For each column in city
            city[i][j]+=vol_by_case(V, city)    # Increment cell by volume per case
    return city

def distribute_water_to_neighbors(city, city0, y, x):
    """
    Distribute water from a cell to its lower neighbors.

    Args:
    - city (matrix/numpy array): Current city matrix.
    - city0 (matrix/numpy array): Original city matrix.
    - y (int): Row index of the cell.
    - x (int): Column index of the cell.

    Returns:
    - matrix (numpy array/list in a list): Matrix representing water redistribution.
    """
    empty_matrix = generate_flat_city(len(city), len(city[0]))

    eau = city[y][x] - city0[y][x]

    # If water is zero or negative, return an empty matrix
    if isclose(eau, 0, atol=1e-3) or eau <= 0:
        return empty_matrix

    case0 = city0[y][x]
    count = 0
    neighbors = []

    # Find lower neighbors for water redistribution
    for i in [-1, 0, 1]:
        for j in [-1, 0, 1]:
            ny, nx = y + i, x + j
            if (
                0 <= ny < len(city)
                and 0 <= nx < len(city[0])
                and not isclose(city[y][x], city[ny][nx], atol=1e-3)
                and city[ny][nx] < city[y][x]
            ):
                count += 1
                neighbors.append((ny, nx))

    # If no lower neighbors, return an empty matrix
    if count == 0:
        return empty_matrix

    eau_par_case = eau / count

    # Redistribute water to neighbors
    for ny, nx in neighbors:
        diff = min(eau_par_case, city[y][x] - city[ny][nx])
        if city[ny][nx] < case0 - eau_par_case:
            empty_matrix[y][x] -= eau_par_case
            empty_matrix[ny][nx] += eau_par_case
        elif case0 + eau_par_case > city[ny][nx]:
            eau_add = (case0 + eau_par_case - city[ny][nx]) / 2
            empty_matrix[y][x] -= eau_add
            empty_matrix[ny][nx] += eau_add
        else:
            n = 2
            while n <= count:
                if case0 + n * eau_par_case > city[ny][nx]:
                    eau_add = (case0 + n * eau_par_case - city[ny][nx]) / 2
                    empty_matrix[y][x] -= eau_add
                    empty_matrix[ny][nx] += eau_add
                    break
                n += 1

    return empty_matrix

def perform_single_iteration(city, city0):
    """
    Perform a single iteration of water redistribution in the city.

    Args:
    - city (list): Current city matrix.
    - city0 (list): Original city matrix.

    Returns:
    - new_city (list): Updated city matrix after a single iteration.
    """
    new_city = copy.deepcopy(city)

    for i in range(len(city)):
        for j in range(len(city[0])):
            matrix = distribute_water_to_neighbors(city, city0, i, j)

            # Add matrix values to the new city
            for a in [-1, 0, 1]:
                for b in [-1, 0, 1]:
                    ny, nx = a + i, b + j
                    if 0 <= ny < len(city) and 0 <= nx < len(city[0]):
                        new_city[ny][nx] += matrix[ny][nx]

    
    return new_city

def simulate_water_flow(city0, V, max_iterations=20):
    """
    Simulate the flow of water in the city over multiple iterations.

    Args:
    - city0 (list): Original city matrix.
    - V (int): Initial volume of water to be distributed.
    - max_iterations (int): Maximum number of iterations.

    Returns:
    - city (list): Final city matrix after water simulation.
    """
    city = copy.deepcopy(city0)
    city = filled_city(city, V)

    for iteration in range(1, max_iterations + 1):
        print(f"Iteration {iteration}")
        previous_city = copy.deepcopy(city)
        city = perform_single_iteration(city, city0)

        # Check for convergence
        if np.array_equal(city, previous_city):
            print("Convergence reached.")
            break

    return city
def mywater(city, city0):
    '''
    function separates the water from the total height by calculating the difference between corresponding elements in two 2D lists (city and city0)
    
    arg: 
    - city0 (list): Original city matrix.
    - city (list): Current city matrix.
    
    return:
    - city_water: matrix of the height of water
    '''
    # Create city_diff 2D array for difference of each element in city and city0
    city_diff=[[0 for i in range(0, len(city[0]))] for j in range(0, len(city))]
    
    # Calculate the difference of each element in city and city0
    for i in range(len(city)):
        for j in range(len(city[0])):
            city_diff[i][j] = city[i][j]-city0[i][j]
    return city_diff

def display_water(city, city0, num):
    '''
    this function takes the city with water and without and a number of iteration 
    Args:
    - city0 (list): Original city matrix.
    - city (list): Current city matrix.
    - num : number of iteration wanted

    it returns an animation of the flow of the water for num iteration
    '''

    # create a new figure and axes for plotting
    fig, ax = plt.subplots()
    
    # create initial water matrix and display it
    water = mywater(perform_single_iteration(city, city0), city0)
    img = ax.imshow(water, cmap='Blues')
    
    # create a colorbar for the plot
    cbar=fig.colorbar(img)
    
    # define the animation function
    def animate(num):
        # perform one iteration of the simulation
        nonlocal city
        nonlocal city0
        city = perform_single_iteration(city, city0)
        
        # calculate new water matrix based on simulation
        new_water = mywater(city, city0)
        
        # update the displayed water matrix
        img.set_array(new_water)
        
        # return the updated image
        return img
    
    # create and display the animation
    anim = animation.FuncAnimation(fig, animate,frames= num, interval=100, repeat=False)
    plt.title('Water Simulation')
    plt.show()
# FLOODVIZ ..................................................................<img src="./images/logo.PNG" alt="image" width="90" height="90">

![Floodviz](./images/floodviz.JPG)
## Description of the project:
- The goal of this project is to create a software that predicts the stagnation of the water when we consider the quantity of water that is poured on rainy days to advise the best places to put the sewers
in this code you give the precipitation (in mm) for a certain duration, the cartography.

Client Target This software is developeed for cities administration and help locals representative and  civil engineer to bettre place the sewer. This software aims to enhence sanitary ``


## How to use the software

 If you want to use **Floodviz** you need to: 
 - open your terminal 
 - In your terminal, clone the folder to your local repo with `git clone https://gitlab-student.centralesupelec.fr/alix.nay/clash-of-cells.git`
 - Open the folder with VsCode

- install ` mathplotlib, numpy, and random, and Tkinter` to do so copy the following code in your terminal:
``` 
for macos: pip3 install <module_name>
for windows: pip install <module_name>

```
- Once you execute the code, the interface will appear. Now you can select the city you want, and select the amount of rain you want (be careful with the unit, you have to enter an amout in m**3 ). 
- then enjoy the simulation 

How to understand the result: 
- The final result is a map of the amount of water on each square meter of the city/space studied. You can see blue tiles, and the shade provide an indication of the level of water. Dark blue corresponds to a higher level of water, and white corresponds to the absence of water. We can see that the map of the town and the map of the amounts of water correspond : high buildings or hills are not submerged, whereas low altitude spaces are submerged.

## Upgraded beta version
We have also made another version of our software, with more parameters as inputs. To open it, you first need to open the folder you just opened with VS code(having cloned it), and then write in your terminal : ' git checkout JeanF'. You can then run the main program.
In this version, you are asked :
- the city you want
- the amount of rain(precipitation), in mm
- the max number of iterations (an integer)
- the sewer positions and maximum flow with the form : [(y_sewer1, x_sewer1, maximum flow sewer1), (y_sewer2, x_sewer2, maximum flow sewer2)]
- the maximum capacity of the sewers, in m**3
- the soil absorption, in L/m²

### Demo:
Here, a demo of our sotfware:``
[![demo](./images/Coding_weeks.mp4)]

## Organization of the  Floodviz Project:
This project is divided into several **Sprints**  and **tasks**. The concept of a sprint refers to the [agile methodology](https://en.wikipedia.org/wiki/Agile_software_development), where a sprint is a time interval during which the project team completes a set of tasks.
All those **Features**  and **sprints** are part of a very specific coding method: the [MVP methodology](https://thetribe.io/glossaire-mvp-methode-minimum-viable-product/#:~:text=En%20bref%2C%20le%20MVP%20est,fonction%20des%20besoins%20des%20consommateurs).


 
 Clic on the following link to see more of the organisation
 
 
- [**Sprint 1**](./sprints/sprint1.md) Create the world (First day)
- [**Sprint 2**](./sprints/sprint2.md)  Logics (First and second day)
- [**Sprint 3**](./sprints/sprint3.md) Displaying the simulation
- [**Sprint 4**](./sprints/sprint4.md) Readme and presentation
- [**Sprint 5**](./sprints/sprint5.md) Improvement

At the beginning of each day, we define the goal to reach by the end of the day. This also implies to define the task distribution and a clear schedule.

Here an expemple of a typical day with us:

<img src="./images/schedule2.png" alt="image" width="500" height="auto">

 

import tkinter as tk
from tkinter import ttk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt

def show_values(precipitation, city_type):
    print("Precipitation:", precipitation)
    print("City Type:", city_type)
    # You can add the logic to display the city on the canvas here

def submit_values(root, grid_size_entry, listeCombo):
    precipitation = int(grid_size_entry.get())
    city_type = listeCombo.get()
    show_values(precipitation, city_type)

    # Hide the current window
    root.withdraw()

    # Create the next window
    root2 = tk.Toplevel(root)
    root2.title("Grid Properties")

    cities_label = tk.Label(root2, text="Enter City:")
    cities_label.grid(row=0, column=0)
    listeProduits = ["city1", "city2", "city3", "city4", "city5", "hill", "hill_village", "eiffel", "randomcity", "randomvillage"]
    listeCombo = ttk.Combobox(root2, values=listeProduits)
    listeCombo.grid(row=0, column=1)

    grid_size_label = tk.Label(root2, text="Enter Precipitation in m3:")
    grid_size_label.grid(row=2, column=0)
    grid_size_entry = tk.Entry(root2)
    grid_size_entry.grid(row=2, column=1)

    submit_button = tk.Button(root2, text="Submit", command=lambda: show_values(int(grid_size_entry.get()), listeCombo.get()))
    submit_button.grid(row=5, columnspan=2)

    result_label = tk.Label(root2, text="")
    result_label.grid(row=6, columnspan=2)

    quit_button = tk.Button(root2, text="Generate Simulation", command=root2.destroy)
    quit_button.grid(row=10, columnspan=2)

    # Matplotlib figure and canvas for displaying the city, matplotlib is placed in a canvas tkinter window
    fig, ax = plt.subplots()
    canvas = FigureCanvasTkAgg(fig, master=root2)
    canvas_widget = canvas.get_tk_widget()
    canvas_widget.grid(row=7, columnspan=2, sticky="nsew")

    # Reconfiguration of the grid for everything to fit in one tkinter window
    root2.grid_rowconfigure(7, weight=1)
    root2.grid_columnconfigure(1, weight=1)

    # Show the new window
    root2.deiconify()

def gui_floodviz():
    root = tk.Tk()
    root.title("WaterViz's Game of Life")

    description = tk.Label(root, text="Welcome to DemoViz. This program allows you to simulate the rainfall that falls evenly on a city or landscape and animates its descent until it reaches its local extremum meaning it can't descend more.")
    description.grid(row=0, column=0)

    description2 = tk.Label(root, text="Please close this window")
    description2.grid(row=1, column=0)

    quit_button = tk.Button(root, text="Next", command=lambda: submit_values(root, grid_size_entry, listeCombo))
    quit_button.grid(row=2, columnspan=2)

    # Hide the next window until the user clicks "Next"
    root.withdraw()

    grid_size_label = tk.Label(root, text="Enter Precipitation in m3:")
    grid_size_label.grid(row=2, column=0)
    grid_size_entry = tk.Entry(root)
    grid_size_entry.grid(row=2, column=1)

    cities_label = tk.Label(root, text="Enter City:")
    cities_label.grid(row=0, column=0)
    listeProduits = ["city1", "city2", "city3", "city4", "city5", "hill", "hill_village", "eiffel", "randomcity", "randomvillage"]
    listeCombo = ttk.Combobox(root, values=listeProduits)
    listeCombo.grid(row=0, column=1)

    root.mainloop()

gui_floodviz()

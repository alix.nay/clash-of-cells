import tkinter as tk
from tkinter import ttk
from cities import *
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from algorithm_and_animation import *
from generate_randomcity import *
from generate_randomhill import *


def gui_floodviz():
    '''
    function that displays the GUI of the flooding visualisation simulation with the choice of parameters and the display of the animation of the rainfall
    no args
    no return
    '''
    global values, cities, iterations_entry, precipitation_entry, Combo_cities, result_label, canvas #creation of all the different variables
    
    #use of a dictionary to store the values that the user will enter because a dictionary is unmutable and its values can be saved even if values are entered inside a function or a root mainloop
    values = {}
    
    #function gets called when user inputs his values and stores his inputs and displays the city he chose
    def show_values():

        try:   #try only runs if the code doesnt return an error
            #values entered by the user are stored in a dictionary
            values["precipitation"] = int(precipitation_entry.get())
            values["iteration"] = int(iteration_entry.get())
            values["cities"] = Combo_cities.get()
    
            #creation of parameter dictionary to store the inputs by user
            param = {}
            
            #function gets called when user inputs his values to create own city and stores his inputs 
            def show_param_city():
                param["rows"] = int(x_axis_entry.get())
                param["columns"] = int(y_axis_entry.get())
                param["tallbuilding"] = float(tallbuilding_prob_entry.get())
                param["horiz_road"] = float(horizontal_road_entry.get())
                param["vert_road"] = float(vertical_road_entry.get())
                param["incline"] = float(incline_prob_entry.get())
                param["green"] = float(greenspace_prob_entry.get())
                param["road"] = float(random_road_entry.get())
                param["choice"] = choiceroad_Combo.get()
                
            #function gets called when user inputs his values to create own hill and stores his inputs
            def show_param_hill():
                param["rows"] = int(x_axis_entry.get())
                param["columns"] = int(y_axis_entry.get())
                param["numhills"] = int(num_hills_entry.get())
                param["maxheight"] = float(max_hillheight_entry.get())
                param["house"] = float(house_prob_entry.get())

            #displays the inputs entered
            result_label.config(text=f"City Type: {values['cities']}\nPrecipitation (m3): {values['precipitation']}") #

            #the following happens only when user wants to create own city or hill
            if values["cities"] == "randomcity" or values["cities"] == "randomvillage":
                if values["cities"] == "randomcity": #for a randomcity
                    ownmap_window = tk.Tk()
                    ownmap_window.title("Random city generation parameters (Close window after pressing Next)")
                    
                    #all labels user has to enter to create own city
                    x_axis_label = tk.Label(ownmap_window, text="Number of columns (x axis) : ")
                    x_axis_label.grid(row=0, column=0)
                    x_axis_entry = tk.Entry(ownmap_window)
                    x_axis_entry.grid(row=0, column=1)
                    
                    y_axis_label = tk.Label(ownmap_window, text="Number of rows (y axis) : ")
                    y_axis_label.grid(row=1, column=0)
                    y_axis_entry = tk.Entry(ownmap_window)
                    y_axis_entry.grid(row=1, column=1)
                    
                    tallbuilding_prob = tk.Label(ownmap_window, text="Probability of tall buildings (15 to 30 meters) (0.2 recommended) : ")
                    tallbuilding_prob.grid(row=2, column=0)
                    tallbuilding_prob_entry = tk.Entry(ownmap_window)
                    tallbuilding_prob_entry.grid(row=2, column=1)
                    
                    horizontal_road_prob = tk.Label(ownmap_window, text="Probability of horizontal full roads (if 'full' chosen) (0.3 recommended) : ")
                    horizontal_road_prob.grid(row=3, column=0)
                    horizontal_road_entry = tk.Entry(ownmap_window)
                    horizontal_road_entry.grid(row=3, column=1)
                    
                    vertical_road_prob = tk.Label(ownmap_window, text="Probability of vertical full roads (if 'full' chosen) (0.3 recommended): ")
                    vertical_road_prob.grid(row=4, column=0)
                    vertical_road_entry = tk.Entry(ownmap_window)
                    vertical_road_entry.grid(row=4, column=1)
                    
                    incline_prob = tk.Label(ownmap_window, text="Probability of inclines on roads : (0.3 recommended)")
                    incline_prob.grid(row=5, column=0)
                    incline_prob_entry = tk.Entry(ownmap_window)
                    incline_prob_entry.grid(row=5, column=1)
                    
                    greenspace_prob = tk.Label(ownmap_window, text="Probability of green spaces (3 meter trees) (0.02 recommended): ")
                    greenspace_prob.grid(row=6, column=0)
                    greenspace_prob_entry = tk.Entry(ownmap_window)
                    greenspace_prob_entry.grid(row=6, column=1)
                    
                    random_road_prob = tk.Label(ownmap_window, text="Probability of random roads (if 'random' chosen) (0.01/0.02 recommended to have lesser roads) : ")
                    random_road_prob.grid(row=7, column=0)
                    random_road_entry = tk.Entry(ownmap_window)
                    random_road_entry.grid(row=7, column=1)
                    
                    choice_road = tk.Label(ownmap_window, text="Choose what type of roads you want ('full' roads or 'random' roads with random lengths) : ")
                    choice_road.grid(row=8, column=0)
                    listchoice=["full", "random"]
                    choiceroad_Combo = ttk.Combobox(ownmap_window, values=listchoice)
                    choiceroad_Combo.grid(row=8,column=1)
                    
                    #when user presses on submit, he stores his values
                    submit_button = tk.Button(ownmap_window, text="Submit", command=show_param_city)
                    submit_button.grid(row=9, columnspan=2)

                    #this ends the window and continues on
                    quit_button = tk.Button(ownmap_window, text="Next", command=ownmap_window.quit)
                    quit_button.grid(row=11, columnspan=2)
                    
                    ownmap_window.mainloop()
                    
                elif values["cities"] == "randomvillage": #for a randomhill
                    ownmap_window = tk.Tk()
                    ownmap_window.title("Random hill with village generation parameters (Close window after pressing Next)")
                    
                    #all labels user has to enter to create own hill
                    x_axis_label = tk.Label(ownmap_window, text="Number of columns (x axis) : ")
                    x_axis_label.grid(row=0, column=0)
                    x_axis_entry = tk.Entry(ownmap_window)
                    x_axis_entry.grid(row=0, column=1)
                    
                    y_axis_label = tk.Label(ownmap_window, text="Number of rows (y axis) : ")
                    y_axis_label.grid(row=1, column=0)
                    y_axis_entry = tk.Entry(ownmap_window)
                    y_axis_entry.grid(row=1, column=1)
                    
                    num_hills = tk.Label(ownmap_window, text="Number of hills : ")
                    num_hills.grid(row=2, column=0)
                    num_hills_entry = tk.Entry(ownmap_window)
                    num_hills_entry.grid(row=2, column=1)
                    
                    max_hillheight = tk.Label(ownmap_window, text="Maximmum height of hill: ")
                    max_hillheight.grid(row=3, column=0)
                    max_hillheight_entry = tk.Entry(ownmap_window)
                    max_hillheight_entry.grid(row=3, column=1)
                                    
                    house_prob = tk.Label(ownmap_window, text="Probability of houses on top of the hills (5x5 house) : ")
                    house_prob.grid(row=4, column=0)
                    house_prob_entry = tk.Entry(ownmap_window)
                    house_prob_entry.grid(row=4, column=1)
                    
                    submit_button = tk.Button(ownmap_window, text="Submit", command=show_param_hill)
                    submit_button.grid(row=5, columnspan=2)

                    quit_button = tk.Button(ownmap_window, text="Next", command=ownmap_window.quit)
                    quit_button.grid(row=7, columnspan=2)

                    ownmap_window.mainloop()
                    
            if values["cities"] == "randomcity":
                cities["randomcity"] = generate_city(param["rows"],param["columns"],param["tallbuilding"],param["horiz_road"],param["vert_road"],param["incline"],param["green"],param["road"],param["choice"])
            if values["cities"] == "randomvillage":
                cities["randomvillage"] = generate_hill_village(param["rows"],param["columns"],param["numhills"],param["maxheight"],param["house"])
            
            # Chooses the type of cmap if a city landscape or if a hilly landscape
            if values["cities"] == "hill" or values["cities"] == "hill_village" or values["cities"] == "randomvillage":
                cmap = "terrain"
            if values["cities"] == "city1" or values["cities"] == "city2" or values["cities"] == "city3" or values["cities"] == "city4" or values["cities"] == "city5" or values["cities"] == "eiffel" or values["cities"] == "randomcity":
                cmap = "binary"
                
            # Display the city using Matplotlib inside a tkinter canvas window
            plt.clf()
            plt.imshow(S(cities[values["cities"]]), cmap=cmap, origin='lower')
            plt.colorbar(label='Height')
            canvas.draw()
            canvas.flush_events() 
            
            #description of the city chosen under the city
            description_text = ""
            if values["cities"] == "city1":
                description_text = "city1 is a randomly generated city 100x100 with pretty basic parameters to make a plausible city (new york, LA style)"
            elif values["cities"] == "city2":
                description_text = "city2 is a randomly generated city 100x100 without any green areas"
            elif values["cities"] == "city3":
                description_text = "city3 is a randomly generated city 100x100 with more tall buildings between 15 and 30 meters."
            elif values["cities"] == "eiffel":
                description_text = "eiffel is a randomly generated city 75x75 that resembles Paris with the representation of the Eiffel Tower in the middle"
            elif values["cities"] == "city4":
                description_text = "city4 is a randomly generated city 100x100 without green areas and has randomly lengthy roads"
            elif values["cities"] == "city5":
                description_text = "city5 is a randomly generated city 100x100 and has randomly lengthy roads"
            elif values["cities"] == "hill":
                description_text = "hill is a randomly generated hill map 100x100 constituted of tons of different hills on top of each other"
            elif values["cities"] == "hill_village":
                description_text = "hill_village (100x100) is a combination of randomly generated hills that don't surpass 5 meters with a possible 5x5 house on top of\n the hill that is five meters tall"    
            elif values["cities"] == "randomcity":
                description_text = "Your own city" 
            elif values["cities"] == "randomvillage":
                description_text =  "Your own village" 
            
            description.config(text=description_text)
    
        except ValueError:  #if errror is returned, this code actviates, this makes sure that the precipitation is always a numeric value
            errorwindow = tk.Tk()
            errorwindow.title("Error")
            error_label = tk.Label(errorwindow, text="Error : Please enter valid numeric values precipitation.", fg="red")
            error_label.grid(row=0, column=0)
            errorwindow.mainloop()
    
    #the first tkinter window which welcomes the user and explains briefly the project
    introwindow = tk.Tk()
    introwindow.title("WaterViz's Game of Life")
    
    description = tk.Label(introwindow,text = "Welcome to DemoViz. This program allows you to simulate the rainfall that falls evenly on a city or landscape and animates its descent from until it reaches its local extremum meaning it can't descend more.")
    description.grid(row = 0, column = 0)
    
    description2 = tk.Label(introwindow,text = "Please close this window")
    description2.grid(row = 1, column = 0)
    
    #quit button to go to the next window
    quit_button = tk.Button(introwindow, text="Next", command=introwindow.quit)
    quit_button.grid(row=2, columnspan=2)
    introwindow.mainloop()

    #second tkinter window which is where the user enters the different parameters (rainfall precipitation and choice of city)
    parameterwindow = tk.Toplevel(introwindow)
    
    parameterwindow.title("Grid Properties")

    #all the dfferent labels in this window
    #combobox used for the user to choose from a list which he wants
    cities_label = tk.Label(parameterwindow, text="Enter City :")
    cities_label.grid(row=0, column=0)
    list_cities=["city1", "city2","city3","city4","city5","hill","hill_village","eiffel","randomcity","randomvillage"]
    Combo_cities = ttk.Combobox(parameterwindow, values=list_cities)
    Combo_cities.grid(row=0,column=1)

    precipitation_label = tk.Label(parameterwindow, text="Enter Precipitation in m3:")
    precipitation_label.grid(row=2, column=0)
    precipitation_entry = tk.Entry(parameterwindow)
    precipitation_entry.grid(row=2, column=1)

    iteration_label = tk.Label(parameterwindow, text="Enter Iterations :")
    iteration_label.grid(row=3, column=0)
    iteration_entry = tk.Entry(parameterwindow)
    iteration_entry.grid(row=3, column=1)

    #this runs the function show_values that allows to store the inputs of the user
    submit_button = tk.Button(parameterwindow, text="Submit", command=show_values)
    submit_button.grid(row=4, columnspan=2)

    #shows the inputs entered by user
    result_label = tk.Label(parameterwindow, text="")
    result_label.grid(row=5, columnspan=2)

    #description of the city chosen
    description = tk.Label(parameterwindow, text="")
    description.grid(row = 7, columnspan = 2)
            
    quit_button = tk.Button(parameterwindow, text="Generate Simulation", command=parameterwindow.destroy)
    quit_button.grid(row=8, columnspan=2)
    
    # Matplotlib figure and canvas for displaying the city, matplotlib is placed in a canvas tkinter window
    fig, ax = plt.subplots()
    canvas = FigureCanvasTkAgg(fig, master=parameterwindow)
    canvas_widget = canvas.get_tk_widget()
    canvas_widget.grid(row=6, columnspan=2, sticky="nsew")  

    # Reconfiguration of the grid for everything to fit in one tkinter window
    parameterwindow.grid_rowconfigure(6, weight=1)  #canvas with city/hill map is on row 6, so we precise a value of 6 in the reconfiguration
    parameterwindow.grid_columnconfigure(6, weight=1)
    
    #End of the parameter window loop
    parameterwindow.mainloop()

    #Display of the water simulation after the end of the parameterwindow mainloops
    city0 = S(cities[values["cities"]]) #city without water that user entered
    city1 = copy.deepcopy(city0)
    city1 = filled_city(city1, values["precipitation"]) #city with water
    display_water(city1, city0, values["iteration"])


 
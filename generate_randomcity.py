import numpy as np
import random as rd
import matplotlib.pyplot as plt

#function which creates a randomly generated plausible city with many different feature
def generate_city(rows, cols, skyscraper_prob,horizontal_road_prob,vertical_road_prob,incline_prob,green_space_prob,road_prob,road_choice,x=0):
    '''
    function generated a random city with the given grid, probabilities and choice of user
    
    args :
    - rows (int) : number of rows (y-axis)
    - cols (int) : number of columns (x-axis)
    - skyscraper_prob (float) : probability of generating tall buildings
    - horizontal_road_prob (float) : probability of generating tall buildings
    - vertical_road_prob (float) : probability of generating tall buildings
    - incline_prob (float) : probability of generating tall buildings
    - green_space_prob (float) : probability of generating tall buildings
    - road_prob (float) : probability of generating tall buildings
    - road_choice (str) : choice of road (full roads that stretch the whole matrix or random roads with random lengths and positions)
    - x (float) : the second element of the list inside for each element of the city matrix, equals to 0 here because no water

    returns:
    - the randomly generated city with the different parameters entered
    '''
    city = [[[0, x] for _ in range(cols)] for _ in range(rows)]

    # Create roads (height = 0)
    for i in range(rows):
        for j in range(cols):
            city[i][j][0] = 0

    # Create buildings
    for i in range(0, rows ):
        for j in range(0, cols ):
            if rd.random() < skyscraper_prob:
                # Create Skyscraper between 15 and 30 meters
                height = rd.uniform(15, 30)
            else:
                # Create Small building between 15 to 30 meters
                height = rd.uniform(10, 15)

            # Set building height
            city[i][j][0] = height
    
    if road_choice == "random":
        #Create random roads with a given probability 
        for i in range(1, rows,2):
            for j in range(1, cols,2):
                if rd.random() < road_prob :
                    # Initialize the road with a random direction
                    direction = rd.choice(['vertical', 'horizontal'])
                    road_length = rd.randint(10, cols+rows//2)

                    if direction == 'vertical':
                        for k in range(road_length):  #makes a continuous road with the length 
                            if i + k < rows:
                                city[i + k][j][0] = 0
                    elif direction == 'horizontal':
                        for k in range(road_length):  #makes a continuous road with the length
                            if j + k < cols:
                                city[i][j + k][0] = 0

        #Creates inclines in the road to indicate that the road goes up and down like plausible roads
        for i in range(rows):
            for j in range(cols):
                if city[i][j][0] == 0:  # there is a road
                    if rd.random() < incline_prob:
                        index = rd.randint(0, min(rows, cols) // 2)
                        incline = rd.uniform(0.1, 1)  #inclines can go from 0.1 meters to 1 meters
                        for k in range(index):
                            if i + k < rows:  #checks if index doesnt go out of matrix
                                city[i + k][j][0] += incline
                            if j + k < cols:  #checks if index doesnt go out of matrix
                                city[i][j + k][0] += incline
    
    elif road_choice == "full":
        #Create fully straight horizontal roads
        for i in range(1,rows,2):
            if rd.random() < horizontal_road_prob:
                for j in range(0,cols):
                    # if rd.random() < horizontal_road_prob:
                        city[i][j][0] = 0
        
        #Create fully straight vertical raods 
        for i in range(1,cols,2):
            if rd.random() < vertical_road_prob:
                for j in range(0,rows):
                    # if rd.random() < vertical_road_prob:
                        city[j][i][0] = 0
        
        
        #Creation of light inclines on the horizontal roads
        for i in range(rows):
            
                if city[i][0][0] == 0:  #there is a road on the row thus
                    
                    if rd.random()<incline_prob:
                        index = rd.randint(0,cols//2)
                        incline = rd.uniform(0.1,1)
                        for j in range(0,index):
                            city[i][j][0] += incline
 
                    if rd.random() < incline_prob:
                        index = rd.randint(cols//2+1,cols)
                        incline = rd.uniform(0.1,1)
                        for j in range(index,cols):
                            city[i][j][0] += incline
                            
        #Creation of light inclines on the vertical roads
        for i in range(cols):
        
                if city[0][i][0] == 0:  #there is a road on the row thus
                    
                    if rd.random() < incline_prob:
                        index = rd.randint(0,rows//2)
                        incline = rd.uniform(0.1,1)
                        for j in range(0,index):
                            city[j][i][0] += incline
                            
                    if rd.random()<incline_prob:
                        index = rd.randint(rows//2+1,rows)
                        incline = rd.uniform(0.1,1)
                        for j in range(index,rows):
                            city[j][i][0] += incline
                
    #Creation of green spaces
    for i in range(rows):
        for j in range(cols):
            if rd.random() < green_space_prob and city[i][j][0] > 1 :  #checks that the green space isnt on the road
                city[i][j][0] = 3 # Assign a special value to represent green spaces ( 3 meter tree )
   
    return city

def print_city(city):
    for row in city:
        print(row)

#function that converts the matrix with the two element list as elements into a matrix with int elements 
def S(city):
    Scity= [[ 0 ] * len(city[0]) for _ in range(len(city))]
    for i in range(len(city)):
        for j in range(len(city[0])):
            Scity[i][j]= city[i][j][0] + city[i][j][1]
    return Scity

def display_city(city):    
    plt.imshow(city, cmap='binary', origin='lower')
    plt.colorbar(label='Height')
    plt.show()
    
#function that writes the full matrix in a text file because the matrix is too large
def write_city(city):
    
    with open("city10.txt","w") as f:
        f.write(str(city))
### Sprint 3: displaying the simulation ........................................................... <img src="./logo.PNG" alt="image" width="90" height="90">
- `task 9 `: creating an interface using **Tkinter** that allows us and later the clients to 
input our different initial conditions : 
    city
    the amount of rain in m**3
    and then display the simulation (`Arthur and Julien`)

- `task 10 `: creating a window that displays simulation on a canvas widget (`Arthur`)

 direct access to the [**Sprint 4**](./sprint4.md) Readme and presentation
 ## Sprint 1: create the world .................................................. <img src="./logo.PNG" alt="image" width="90" height="90">
- `task 1` : find a way to generate 2 matrices (`jean`). One represents the city without water and is a list of lists with the heights of each square meter of the map. The second one represents the amount of water on each square meter of the map, and is a list of lists with the height of water on each square meter.
-  `task 2`: creation of different city maps with different landforms. Minimum of 10 with a `generate_city` function  (`julien and Yasmine`)
- `Task 3 ` : create different test to test all our functions ( `Alix`)

direct access to the [**Sprint 2**](./sprint2.md)  Logics (First and second day)
### Sprint 2: logics ...................................................... <img src="./logo.PNG" alt="image" width="90" height="90">
- `task 4`: creating a function that  return the neighboring cells
 of an x,y cel (`Yasmine`)

- `task 5`: creating a function that returns the points of the neighboring cells of which the combined heights are inferior to that of the x,y point.(`all`)

- `task 6`: creating `the ditribute_water_height `function which distributes the water height of the x,y points homogeneously to points returned by the previous function. This distribution must be done in a copy of the empty matrices so that we do not count twice the same water. this and to the copy’s point there new water level. make sure that ponds are able to form evenly.
this might lead to us creating the trace back function that checks for every	point where the combined heights is superior to neighbors with WH=0 and WS<WG that distributes evenly the the difference in all  (`Jean and Joseph`)

- `Trak 7` : the presentation of the movement of the water in the city until stabilized  with **matplotlib** (`jean`)

- `task 8`: run the simulation with run() wich applies all funtions above to every single cell
 with a for loop, n tims. returning every time the array.

direct access to the  [**Sprint 3**](./sprint3.md) Displaying the simulation
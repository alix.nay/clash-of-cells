import ast
import matplotlib.pyplot as plt
import os
import random as rd
from generate_randomcity import *
from generate_randomhill import *

#from generate_randomcity import *

#use of os module to access files in a folder like the text files in the cities_txt folder so all OS systems can use this code
current_dir = os.getcwd()   
folder_name = "cities_txt"

#city 1 extraction
with open(os.path.join(current_dir,folder_name,"city1.txt"),"r") as f: 
    city1 = f.read()
city1 = ast.literal_eval(city1) 
for i in range(len(city1)):  #city1's green spaces had -1 values and they're supposed to be valued at 3
        for j in range(len(city1[0])):
            if city1[i][j][0] == -1:
                city1[i][j][0] = 3
    
#city 2 extraction
with open(os.path.join(current_dir,folder_name,"city2.txt"),"r") as f: 
    city2 = f.read()
city2 = ast.literal_eval(city2) 

#city 3 extraction
with open(os.path.join(current_dir,folder_name,"city3.txt"),"r") as f: 
    city3 = f.read()
city3 = ast.literal_eval(city3) 

#city 4 extraction
with open(os.path.join(current_dir,folder_name,"city4.txt"),"r") as f: 
    city4 = f.read()
city4 = ast.literal_eval(city4)
for i in range(5,30):
    city4[3][i][0] = rd.uniform(10,15)
for i in range(len(city4)):
    city4[i][5][0] = 0

#eiffel tower city extraction
with open(os.path.join(current_dir,folder_name,"eiffel.txt"),"r") as f: 
    eiffel = f.read()
eiffel = ast.literal_eval(eiffel)

#city 5 extraction
with open(os.path.join(current_dir,folder_name,"city5.txt"),"r") as f: 
    city5 = f.read()
city5 = ast.literal_eval(city5)

#hill extraction
with open(os.path.join(current_dir,folder_name,"hill.txt"),"r") as f: 
    hill = f.read()
hill = ast.literal_eval(hill)

#hill village extraction
with open(os.path.join(current_dir,folder_name,"hill_village.txt"),"r") as f: 
    hill_village = f.read()
hill_village = ast.literal_eval(hill_village)




#dictionary containing all the cities' matrixes
cities = {
    #city1 is a randomly generated city with pretty basic parameters to make a plausible city (new york, LA style)
    "city1" : city1,  #100x100
    
    #city2 is a randomly generated city without any green areas
    "city2" : city2,  #100x100
    
    #city3 is a randomly generated city with more tall buildings between 15 and 30 metres.
    "city3" : city3,  #100x100
    
    #eiffel is a randomly generated city that resembles Paris with the represenation of the eiffel tower in the middle
    "eiffel" : eiffel,   #75x75 
    
    #city4 is a randomly generated city without green areas and has randomly lengthy roads
    "city4" : city4,  #100x100
    
    #city5 is a randomly generated city and has randomly lengthy roads
    "city5" : city5,   #100x100
    
    #hill is a randomly generated hill constitued of tons of different hills on top of each other
    "hill" : hill,   #100x100
    
    #hill_village is a combination of randomly generated hill that doesnt surpass 5 meters with a possible 5x5 house on top of the hill that is five meter tall
    "hill_village" : hill_village,   #100x100
    
    #randomcity is a randomly generated city with it being 100x100 matrix with 0.2 probability of tall buildings/skyscrapers, 0.3 probability of creating vertical and horizontal roads, 0.3 probability of creating inclines on the roads and 0.02 probability of creating green spaces that are all three meters (trees) and the type of roads are full roads that go along the full matrix
    "randomcity" : generate_city(100,100,0.2,0.3,0.3,0.3,0.02,0.03,"full"),
    
    #randomvillage is a randomly generated hill with 100x100 matrix with 15 hills generated that go from 1 to 5 meters and has a 0.6 probability of creating houses on the peaks of the hills
    "randomvillage" : generate_hill_village(100,100,15,4,0.6)
}

#print(cities["city3"])

#function that converts the matrix with the two element list as elements into a matrix with int elements by doing the sum of both because second list equals to 0 
def S(city):
    Scity= [[ 0 ] * len(city[0]) for _ in range(len(city))]
    for i in range(len(city)):
        for j in range(len(city[0])):
            Scity[i][j]= city[i][j][0] + city[i][j][1]
    return Scity

# plt.imshow(S(cities["city1"],100,100), cmap='binary', origin='lower')
# plt.colorbar(label='Height')
# plt.show()
    
# plt.imshow(S(cities["city2"],100,100), cmap='binary', origin='lower')
# plt.colorbar(label='Height')
# plt.show()

# plt.imshow(S(cities["city3"],100,100), cmap='binary', origin='lower')
# plt.colorbar(label='Height')
# plt.show()

# plt.imshow(S(cities["eiffel"],75,75), cmap='binary', origin='lower')
# plt.colorbar(label='Height')
# plt.show()

# plt.imshow(S(cities["city4"]), cmap='binary', origin='lower')
# plt.colorbar(label='Height')
# plt.show()

# plt.imshow(S(cities["city5"]), cmap='binary', origin='lower')
# plt.colorbar(label='Height')
# plt.show()

# plt.imshow(S(cities["hill"]), cmap='terrain', origin='lower')
# plt.colorbar(label='Height')
# plt.show()

# plt.imshow(S(cities["hill_village"]), cmap='terrain', origin='lower')
# plt.colorbar(label='Height')
# plt.show()